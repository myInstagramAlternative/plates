import cv2
from matplotlib import pyplot as plt
import numpy as np
import imutils
import easyocr
from os import listdir
from os.path import isfile, join

def getPlateNumb(fullPath):
    pic = fullPath.split("/")[-1]
    print("Obradjujem sliku: " + pic)



    cap = cv2.VideoCapture("/home/ubuntu/tablice/vid/test4.mp4")
    all_results = []
    while(True):
        # Capture frame-by-frame
        ret, img = cap.read()
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

        bfilter = cv2.bilateralFilter(gray, 11, 17, 17)
        edged = cv2.Canny(bfilter, 30, 200)

        try:
            keypoints = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
            contours = imutils.grab_contours(keypoints)
            contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

            location = None
            for contour in contours:
                approx = cv2.approxPolyDP(contour, 10, True)
                if len(approx) == 4:
                    location = approx
                    break

            mask = np.zeros(gray.shape, np.uint8)
            new_image = cv2.drawContours(mask, [location], 0,255, -1)
            new_image = cv2.bitwise_and(img, img, mask=mask)
            #cv2.imshow('new_image',new_image)
        #cv2.imshow('frame',gray)


            (x,y) = np.where(mask==255)
            (x1,y1) = (np.min(x), np.min(y))
            (x2,y2) = (np.max(x), np.max(y))
            cropped_image = gray[x1:x2+1, y1:y2+1]
            #cv2.imshow('cropped_image',cropped_image)


            reader = easyocr.Reader(["rs_latin"])
            result = reader.readtext(cropped_image, rotation_info=[90, 180 ,270], width_ths=1, text_threshold=0.5, blocklist=[".",",","!",'"',"@","#","&","(",")","–","[","{","}","]",":",";","'","?","/","*","`","~","$","^","+","=","<",">","“"])
            if not result:
                print("Ne vidim broj na tablici, slika: " + pic)
            else:
                number = result[0][-2]
                wut = result[0][-1]
                all_results.append(number)
                print(all_results)
                print("Broj tablice: " + number + ", siguran: " + str(wut) + ", slika: " + pic)

        except cv2.error as e:
            print("Nisam uspeo da nadjem konturu: {0}".format(e))
        except ValueError:
            print("Could not convert data to an integer.")
        except:
            print("Unexpected error:", sys.exc_info()[0])
            raise


    return all_results

        # Display the resulting frame
        #cv2.imshow('frame',gray)

        
"""         if cv2.waitKey(1) & 0xFF == ord('q'):
            print(all_results)
            break

    print(all_results)
    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()
     """

    #img = cv2.imread(fullPath)
    
    #plt.imshow(cv2.cvtColor(gray, cv2.COLOR_BGR2RGB))
    #plt.imshow(cv2.cvtColor(cropped_image, cv2.COLOR_BGR2RGB))
    #plt.show()




fullPath = "/blabla"
getPlateNumb(fullPath)
print(all_results)
print("=======================================================================================================================================")

