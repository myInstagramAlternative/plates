import cv2
from matplotlib import pyplot as plt
import numpy as np
import imutils
import easyocr
from os import listdir
from os.path import isfile, join

def getPlateNumb(fullPath):
    pic = fullPath.split("/")[-1]
    print("Obradjujem sliku: " + pic)


    img = cv2.imread(fullPath)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    bfilter = cv2.bilateralFilter(gray, 11, 17, 17)
    edged = cv2.Canny(bfilter, 30, 200)

    try:
        keypoints = cv2.findContours(edged.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        contours = imutils.grab_contours(keypoints)
        contours = sorted(contours, key=cv2.contourArea, reverse=True)[:10]

        location = None
        for contour in contours:
            approx = cv2.approxPolyDP(contour, 10, True)
            if len(approx) == 4:
                location = approx
                break

        mask = np.zeros(gray.shape, np.uint8)
        new_image = cv2.drawContours(mask, [location], 0,255, -1)
        new_image = cv2.bitwise_and(img, img, mask=mask)

        (x,y) = np.where(mask==255)
        (x1,y1) = (np.min(x), np.min(y))
        (x2,y2) = (np.max(x), np.max(y))
        cropped_image = gray[x1:x2+1, y1:y2+1]

        reader = easyocr.Reader(["en"])
        result = reader.readtext(cropped_image)
        if not result:
            print("Ne vidim broj na tablici, slika: " + pic)
        else:
            number = result[0][-2]
            wut = result[0][-1]
            print("Broj tablice: " + number + ", siguran: " + str(wut) + ", slika: " + pic)

    except cv2.error as e:
        print("Nisam uspeo da nadjem konturu: {0}".format(e))
    except ValueError:
        print("Could not convert data to an integer.")
    except:
        print("Unexpected error:", sys.exc_info()[0])
        raise




    #plt.imshow(cv2.cvtColor(gray, cv2.COLOR_BGR2RGB))
    #plt.imshow(cv2.cvtColor(cropped_image, cv2.COLOR_BGR2RGB))
    #plt.show()



path = "/home/fam/Desktop/tablice/"
files = [f for f in listdir(path) if isfile(join(path, f))]
print(files)
for file in files:
    fullPath = path+file
    getPlateNumb(fullPath)
    print("=======================================================================================================================================")

