# Plates recognition (photo && video)
What should I write here?

## Features:
- [x] Image processing
- [x] Video processing

## Tasks:
- [ ] Make service, script
    - [ ] service
    - [ ] script (test/ -- dry run)
- [ ] Make install script
    - [ ] as service (docker, systemctl)
    - [ ] as tool (linux)
- [ ] Build docker image (service and script)
- [ ] Recognize car color (optional)
- [ ] Write data in db (time, car color (optional), possible car plate, possibility (optional), sql3, Mongo, Postgres)
- [ ] Percante of video
- [ ] Configuration (env,file,cmd)
- [ ] Define max video length (Depends on GPU power, probably and on my code quality) `[ --mvl, -m ]`,`MVL=`,`file: possible fyaml`
- [ ] API (if this sh1t ever start to live)
- [ ] UI?

## 'bout:
This project was started cuz I was bored so, there is a great chance that I'll never finish it
